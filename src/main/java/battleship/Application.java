package battleship;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
  public static void main(String[] args) {
    try {
      Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
      System.out.println("database works !!!");
    } catch (ClassNotFoundException ex) {
      System.out.println("error: in database");
      System.exit(0);
    }
    SpringApplication.run(Application.class, args);
  }
}
