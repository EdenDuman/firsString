package battleship.controller;

import java.util.List;
import objects.GameMove;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import sql.Sql;

@RestController
public class GameMoveController {
  @GetMapping("/gameMove")
  public String gameMove() {
    StringBuilder result = new StringBuilder();
    List<GameMove> gameMoves = new Sql().gameMove();

    if (gameMoves == null) {
      result = new StringBuilder("null");
    } else {
      for (GameMove gameMove : gameMoves) {
        result.append(gameMove.toString());
      }
    }

    return result.toString();
  }
}
