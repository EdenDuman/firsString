package battleship.controller;

import java.util.List;
import objects.GameResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import sql.Sql;

@RestController
public class GameResultsController {
  @GetMapping("/gameResults")
  public String gameResults() {
    StringBuilder result = new StringBuilder();
    List<GameResult> gameResults = new Sql().GameResults();

    if (gameResults == null) {
      result = new StringBuilder("null");
    } else {
      for (GameResult gameResult : gameResults) {
        result.append(gameResult.toString());
      }
    }

    return result.toString();
  }
}
