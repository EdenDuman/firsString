package battleship.controller;

import java.util.List;
import objects.Game;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import sql.Sql;

@RestController
public class GamesController {
  @GetMapping("/games")
  public String games() {
    StringBuilder result = new StringBuilder();
    List<Game> games = new Sql().games();

    if (games == null) {
      result = new StringBuilder("null");
    } else {
      for (Game game : games) {
        result.append(game.toString());
      }
    }

    return result.toString();
  }
}
