package battleship.controller;

import java.util.List;
import objects.Log;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import sql.Sql;

@RestController
public class LogController {
  @GetMapping("/log")
  public String users() {
    StringBuilder result = new StringBuilder();
    List<Log> logs = new Sql().log();

    if (logs == null) {
      result = new StringBuilder("null");
    } else {
      for (Log log : logs) {
        result.append(log.toString());
      }
    }

    return result.toString();
  }
}
