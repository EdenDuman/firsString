package battleship.controller;

import java.util.List;
import objects.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import sql.Sql;

@RestController
public class UsersController {
  @GetMapping("/users")
  public String users() {
    StringBuilder result = new StringBuilder();
    List<User> users = new Sql().user();

    if (users == null) {
      result = new StringBuilder("null");
    } else {
      for (User user : users) {
        result.append(user.toString());
      }
    }

    return result.toString();
  }
}
