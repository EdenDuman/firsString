package battleship.controller;

import java.util.List;
import objects.WinAndLose;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import sql.Sql;

@RestController
public class WinAndLoseController {
  @GetMapping("/winAndLose")
  public String winAndLose() {
    StringBuilder result = new StringBuilder();
    List<WinAndLose> winAndLoses = new Sql().winAndLose();

    if (winAndLoses == null) {
      result = new StringBuilder("null");
    } else {
      for (WinAndLose winAndLose : winAndLoses) {
        result.append(winAndLose.toString());
      }
    }

    return result.toString();
  }
}
