package objects;

public class Game {
  private int gameID;
  private int ownerID;
  private String creationTime;
  private boolean isActive;
  private int guestID;
  private String ownerBoard;
  private String guestBoard;

  public Game() {}

  public void setGameID(int gameID) {
    this.gameID = gameID;
  }

  public void setOwnerID(int ownerID) {
    this.ownerID = ownerID;
  }

  public void setCreationTime(String creationTime) {
    this.creationTime = creationTime;
  }

  public void setActive(boolean active) {
    isActive = active;
  }

  public void setGuestID(int guestID) {
    this.guestID = guestID;
  }

  public void setOwnerBoard(String ownerBoard) {
    this.ownerBoard = ownerBoard;
  }

  public void setGuestBoard(String guestBoard) {
    this.guestBoard = guestBoard;
  }

  @Override
  public String toString() {
    String result = "";
    result += "gameID: " + this.gameID + "<br>";
    result += "ownerID: " + this.ownerID + "<br>";
    result += "creationTime: " + this.creationTime + "<br>";
    result += "isActive: " + this.isActive + "<br>";
    result += "guestID: " + this.guestID + "<br>";
    result += "ownerBoard: " + this.ownerBoard + "<br>";
    result += "guestBoard: " + this.guestBoard + "<br>";
    result += "<br>";

    return result;
  }
}
