package objects;

public class GameMove {
  private int id;
  private int gameID;
  private int playerID;
  private String move;
  private boolean isHit;
  private String time;

  public GameMove() {
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setGameID(int gameID) {
    this.gameID = gameID;
  }

  public void setPlayerID(int playerID) {
    this.playerID = playerID;
  }

  public void setMove(String move) {
    this.move = move;
  }

  public void setHit(boolean hit) {
    isHit = hit;
  }

  public void setTime(String time) {
    this.time = time;
  }

  @Override
  public String toString() {
    String result = "";
    result += "ID: " + this.id + "<br>";
    result += "gameID: " + this.gameID + "<br>";
    result += "playerID: " + this.playerID + "<br>";
    result += "move: " + this.move + "<br>";
    result += "isHit: " + this.isHit + "<br>";
    result += "time: " + this.time + "<br>";
    result += "<br>";

    return result;
  }
}
