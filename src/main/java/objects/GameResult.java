package objects;

public class GameResult {
  private int id;
  private int gameID;
  private int playerWon;
  private int playerLost;

  public GameResult() {}

  public void setId(int id) {
    this.id = id;
  }

  public void setGameID(int gameID) {
    this.gameID = gameID;
  }

  public void setPlayerWon(int playerWon) {
    this.playerWon = playerWon;
  }

  public void setPlayerLost(int playerLost) {
    this.playerLost = playerLost;
  }

  @Override
  public String toString() {
    String result = "";
    result += "ID: " + this.id + "<br>";
    result += "gameID: " + this.gameID + "<br>";
    result += "playerWon: " + this.playerWon + "<br>";
    result += "playerLost: " + this.playerLost + "<br>";
    result += "<br>";

    return result;
  }
}
