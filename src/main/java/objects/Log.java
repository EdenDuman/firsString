package objects;

public class Log {
  private int id;
  private String ip;
  private String mac;
  private String time;
  private int action;
  private String mail;
  private String password;
  private int result;

  public Log() {}

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public String getMac() {
    return mac;
  }

  public void setMac(String mac) {
    this.mac = mac;
  }

  public String getTime() {
    return time;
  }

  public void setTime(String time) {
    this.time = time;
  }

  public String getAction() {
    switch (action) {
      case 1:
        return "Registration";
      case 2:
        return "Login";
      case 3:
        return "change Password";
      case 4:
        return "Logout";
      default:
        return "";
    }
  }

  public void setAction(int action) {
    this.action = action;
  }

  public String getMail() {
    return mail;
  }

  public void setMail(String mail) {
    this.mail = mail;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getResult() {
    switch (result) {
      case 0:
        return "Succeeded";
      case 1:
        return "Fails";
      case 2:
        return "You are already logged in";

      default:
        return "";
    }
  }

  public void setResult(int result) {
    this.result = result;
  }

  @Override
  public String toString() {
    String result = "";
    result += "ID: " + this.getId() + "<br>";
    result += "IP: " + this.getIp() + "<br>";
    result += "MAC: " + this.getMac() + "<br>";
    result += "TIME: " + this.getTime() + "<br>";
    result += "ACTION: " + this.getAction() + "<br>";
    result += "MAIL: " + this.getMail() + "<br>";
    result += "PASSWORD: " + this.getPassword() + "<br>";
    result += "RESULT: " + this.getResult() + "<br>";
    result += "<br>";

    return result;
  }
}
