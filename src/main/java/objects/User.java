package objects;

public class User {
  private int id;
  private String nickName;
  private String mail;
  private String password;
  private boolean isActive;
  private String lastSeen;

  public User() {}

  public void setId(int id) {
    this.id = id;
  }

  public void setNickName(String nickName) {
    this.nickName = nickName;
  }

  public void setMail(String mail) {
    this.mail = mail;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public void setActive(boolean active) {
    isActive = active;
  }

  public void setLastSeen(String lastSeen) {
    this.lastSeen = lastSeen;
  }

  @Override
  public String toString() {
    String result = "";
    result += "ID: " + this.id + "<br>";
    result += "nickName: " + this.nickName + "<br>";
    result += "mail: " + this.mail + "<br>";
    result += "password: " + this.password + "<br>";
    result += "isActive: " + this.isActive + "<br>";
    result += "lastSeen: " + this.lastSeen + "<br>";
    result += "<br>";

    return result;
  }
}
