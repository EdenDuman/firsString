package objects;

public class WinAndLose {
  private int id;
  private int playerID;
  private int wins;
  private int lose;

  public WinAndLose() {}

  public void setId(int id) {
    this.id = id;
  }

  public void setPlayerID(int playerID) {
    this.playerID = playerID;
  }

  public void setWins(int wins) {
    this.wins = wins;
  }

  public void setLose(int lose) {
    this.lose = lose;
  }

  @Override
  public String toString() {
    String result = "";
    result += "ID: " + this.id + "<br>";
    result += "playerID: " + this.playerID + "<br>";
    result += "wins: " + this.wins + "<br>";
    result += "lose: " + this.lose + "<br>";
    result += "<br>";

    return result;
  }
}
