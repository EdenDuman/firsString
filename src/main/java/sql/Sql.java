package sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import objects.Game;
import objects.GameMove;
import objects.GameResult;
import objects.Log;
import objects.User;
import objects.WinAndLose;

public class Sql {
  private Connection conn;
  private PreparedStatement ps;
  private ResultSet rs;
  private Statement stmt;

  private Connection createConnection() {
    String url =
        "jdbc:sqlserver://localhost:1433;databaseName=Battleship" + ";integratedSecurity=true";

    try {

      conn = DriverManager.getConnection(url);
    } catch (SQLException ex) {
      System.out.println("error: unable to connect");
      conn = null;
    }

    return conn;
  }

  private void closeConnections() {
    try {
      if (!rs.isClosed()) {
        rs.close();
      }
    } catch (SQLException ex) {
      System.out.println("Error: close connection in SQLConnection class ");
    }
  }

  public List<User> user() {
    try {
      conn = createConnection();
      String sql = "select * from Users";
      stmt = conn.createStatement();
      rs = stmt.executeQuery(sql);

      List<User> result = new ArrayList<>();

      while (rs.next()) {
        User user = new User();
        user.setId(rs.getInt(1));
        user.setNickName(rs.getString(2));
        user.setMail(rs.getString(3));
        user.setPassword(rs.getString(4));
        user.setActive(rs.getBoolean(5));
        user.setLastSeen(rs.getString(6));
        result.add(user);
      }

      closeConnections();

      return result;
    } catch (SQLException ex) {
      System.out.println(" -> " + ex);
      closeConnections();
      return null;
    }
  }

  public List<Game> games() {
    try {
      conn = createConnection();
      String sql = "select * from games";
      stmt = conn.createStatement();
      rs = stmt.executeQuery(sql);

      List<Game> result = new ArrayList<>();

      while (rs.next()) {
        Game game = new Game();
        game.setGameID(rs.getInt(1));
        game.setOwnerID(rs.getInt(2));
        game.setCreationTime(rs.getString(3));
        game.setActive(rs.getBoolean(4));
        game.setGuestID(rs.getInt(5));
        game.setOwnerBoard(rs.getString(6));
        game.setGuestBoard(rs.getString(7));
        result.add(game);
      }

      closeConnections();

      return result;
    } catch (SQLException ex) {
      System.out.println(" -> " + ex);
      closeConnections();
      return null;
    }
  }

  public List<Log> log() {
    try {
      conn = createConnection();
      String sql = "select * from log";
      stmt = conn.createStatement();
      rs = stmt.executeQuery(sql);

      List<Log> result = new ArrayList<>();

      while (rs.next()) {
        Log log = new Log();
        log.setId(rs.getInt(1));
        log.setIp(rs.getString(2));
        log.setMac(rs.getString(3));
        log.setTime(rs.getString(4));
        log.setAction(rs.getInt(5));
        log.setMail(rs.getString(6));
        log.setPassword(rs.getString(7));
        log.setResult(rs.getInt(8));
        result.add(log);
      }

      closeConnections();

      return result;
    } catch (SQLException ex) {
      System.out.println(" -> " + ex);
      closeConnections();
      return null;
    }
  }

  public List<GameMove> gameMove() {
    try {
      conn = createConnection();
      String sql = "select * from GameMove";
      stmt = conn.createStatement();
      rs = stmt.executeQuery(sql);

      List<GameMove> result = new ArrayList<>();

      while (rs.next()) {
        GameMove gameMove = new GameMove();
        gameMove.setId(rs.getInt(1));
        gameMove.setGameID(rs.getInt(2));
        gameMove.setPlayerID(rs.getInt(3));
        gameMove.setMove(rs.getString(4));
        gameMove.setHit(rs.getBoolean(5));
        gameMove.setTime(rs.getString(6));
        result.add(gameMove);
      }

      closeConnections();

      return result;
    } catch (SQLException ex) {
      System.out.println(" -> " + ex);
      closeConnections();
      return null;
    }
  }

  public List<GameResult> GameResults() {
    try {
      conn = createConnection();
      String sql = "select * from GameResults";
      stmt = conn.createStatement();
      rs = stmt.executeQuery(sql);

      List<GameResult> result = new ArrayList<>();

      while (rs.next()) {
        GameResult gameResult = new GameResult();
        gameResult.setId(rs.getInt(1));
        gameResult.setGameID(rs.getInt(2));
        gameResult.setPlayerWon(rs.getInt(3));
        gameResult.setPlayerLost(rs.getInt(4));
        result.add(gameResult);
      }

      closeConnections();

      return result;
    } catch (SQLException ex) {
      System.out.println(" -> " + ex);
      closeConnections();
      return null;
    }
  }

  public List<WinAndLose> winAndLose() {
    try {
      conn = createConnection();
      String sql = "select * from winAndLose";
      stmt = conn.createStatement();
      rs = stmt.executeQuery(sql);

      List<WinAndLose> result = new ArrayList<>();

      while (rs.next()) {
        WinAndLose winAndLose = new WinAndLose();
        winAndLose.setId(rs.getInt(1));
        winAndLose.setPlayerID(rs.getInt(2));
        winAndLose.setWins(rs.getInt(3));
        winAndLose.setLose(rs.getInt(4));
        result.add(winAndLose);
      }

      closeConnections();

      return result;
    } catch (SQLException ex) {
      System.out.println(" -> " + ex);
      closeConnections();
      return null;
    }
  }
}
